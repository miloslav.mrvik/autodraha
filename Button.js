module.exports = (function () {
    "use strict";

    const rpio  = require("rpio");
    const EventEmitter = require("events");

    const P = Symbol('private');
    const TICK = Symbol('tick()');

    const RESISTOR = Object.freeze({
        "OFF": rpio.PULL_OFF,
        "PULL_UP": rpio.PULL_UP,
        "PULL_DOWN": rpio.PULL_DOWN
    });
    const RESISTOR_VALUES = new Set(Object.keys(RESISTOR).map((k) => RESISTOR[k]));

    /**
     * Class which takes care of a GPIO button
     * @event 'change'
     */
    class Button extends EventEmitter {
        constructor (pin, resistor = RESISTOR.OFF) {
            if (!Number.isInteger(pin)) throw new TypeError("Parameter pin must be an int");
            if (!RESISTOR_VALUES.has(resistor)) throw new TypeError("Parameter resistor must be one of Button.RESISTOR.*");

            super();

            this[P] = {
                pin,
                value: null,
                interval: setInterval(() => this[TICK](), 4)
            };

            rpio.open(pin, rpio.INPUT);
            rpio.pud(pin, resistor);
        }

        destructor () {
            clearInterval(this[P].interval);
            rpio.reset(this[P].pin, rpio.PIN_RESET);
            this[P] = null;
        }

        get () {
            return this[P].value;
        }

        [TICK] () {
            const value_raw = rpio.read(this[P].pin);
            const value = (value_raw === rpio.HIGH);
            if (value === this[P].value) return;

            this[P].value = value;
            process.nextTick(() => this.emit("change", value));
        }
    }

    Button.RESISTOR = RESISTOR;

    return Button;
})();