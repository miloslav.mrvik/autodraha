module.exports = (function () {
    "use strict";

    const rpio  = require("rpio");
    const Led  = require("./Led.js");
    const Beep  = require("./Beep.js");
    const Button  = require("./Button.js");

    rpio.init({
        gpiomem: true,         /* Use /dev/gpiomem */
        mapping: 'physical',
        mock: undefined
    });

    const LED_R1 = new Led(22);
    const LED_R2 = new Led(24);
    const LED_R3 = new Led(26);
    const LED_G  = new Led(28);

    const TRACK_1 = new Button(16, Button.RESISTOR.PULL_UP);
    const TRACK_2 = new Button(18, Button.RESISTOR.PULL_UP);

    const BEEP = new Beep(3);

    function onLap (track, val) {
        if (val === false) return;
        console.log(new Date(), "LAP", track);
        BEEP.beep();
    }

    BEEP.alarm();

    const NOOP = function () {};
    var cancel = NOOP;
    
    return {
        cancel () {
            cancel();
            cancel = NOOP;
        },

        newRace () {
            cancel();

            let interval;
            let state = -1;
            let tick = function () {
                if (++state >= 5) return clearInterval(interval);

                switch (state) {
                    case 0: {
                        LED_R1.set(false);
                        LED_R2.set(false);
                        LED_R3.set(false);
                        LED_G.set(false);
                        break;
                    }

                    case 1: {
                        LED_R1.set(true);
                        LED_R2.set(false);
                        LED_R3.set(false);
                        LED_G.set(false);
                        BEEP.beep(250);
                        break;
                    }

                    case 2: {
                        LED_R1.set(true);
                        LED_R2.set(true);
                        LED_R3.set(false);
                        LED_G.set(false);
                        BEEP.beep(250);
                        break;
                    }

                    case 3: {
                        LED_R1.set(true);
                        LED_R2.set(true);
                        LED_R3.set(true);
                        LED_G.set(false);
                        BEEP.beep(250);
                        break;
                    }

                    case 4: {
                        LED_R1.set(false);
                        LED_R2.set(false);
                        LED_R3.set(false);
                        LED_G.set(true);
                        BEEP.beep(1000);
                        break;
                    }
                }
            };

            
            tick();

            interval = setInterval(tick, 1000);
            const lap1 = onLap.bind(this, 1);
            const lap2 = onLap.bind(this, 2);
            TRACK_1.on('change', lap1);
            TRACK_2.on('change', lap2);

            cancel = function () {
                clearInterval(interval);
                TRACK_1.removeListener('change', lap1);
                TRACK_2.removeListener('change', lap2);
            };
        },

        ring () {
            cancel();

            const INT = parseInt(process.env.TROLL, 10);
            const interval = setInterval(() => BEEP.beep(INT), INT * 2);

            cancel = function () { clearInterval(interval); };
        }
    };
})();
