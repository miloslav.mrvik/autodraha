module.exports = {
    "extends": "eslint:recommended",
    "root": true,
    "env": {
        "browser": false,
        "node": true,
        "es6": true
    },
    "rules": {
        "no-console": "off",
        "semi": ["warn", "always"],
        "no-unused-vars": ["warn", { "args": "none", "vars": "local" }],
        "no-cond-assign": ["warn", "except-parens"]
    },
    "parserOptions": {
        "ecmaVersion": 7
    }
};