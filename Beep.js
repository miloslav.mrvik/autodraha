module.exports = (function () {
    "use strict";

    const Led  = require("./Led.js");

    const P = Symbol('private');

    /**
     * Class which takes care of a GPIO BUZZER
     * @event 'change'
     * @event 'alarm-start'
     * @event 'alarm-end'
     */
    class Beep extends Led {
        constructor (pin) {
            super(pin);

            this[P] = {
                timeout: null
            };
        }

        destructor () {
            clearTimeout(this[P].timeout);
            this[P] = null;

            super.destructor();
        }

        beep (length = 100) {
            if (!(Number.isFinite(length) && (length >= 0))) throw new TypeError("Parameter length must be finite number >= 0");

            clearTimeout(this[P].timeout);
            this[P].timeout = setTimeout(() => this.set(false), length);

            this.set(true);
        }

        alarm () {
            const b = () => this.beep();

            this.emit('alarm-start');

            b();
            setTimeout(b, 330).unref();
            setTimeout(b, 660).unref();
            setTimeout(() => {
                b();
                this.emit('alarm-end');
            }, 990).unref();
        }
    }

    return Beep;
})();