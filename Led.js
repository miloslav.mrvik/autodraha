module.exports = (function () {
    "use strict";

    const rpio  = require("rpio");
    const EventEmitter = require("events");

    const P = Symbol('private');

    class Led extends EventEmitter {
        constructor (pin) {
            if (!Number.isInteger(pin)) throw new TypeError("Parameter pin must be an int");
            super();

            this[P] = {
                pin,
                value: false,
            };

            rpio.open(pin, rpio.OUTPUT, rpio.LOW);
            rpio.pud(pin, rpio.PULL_OFF);
        }

        destructor () {
            rpio.reset(this[P].pin, rpio.PIN_RESET);
            this[P] = null;
        }

        get () {
            return this[P].value;
        }

        set (value) {
            if (typeof value !== 'boolean') throw new TypeError("Parameter value must be boolean");

            if (value === this[P].value) return;
            rpio.write(this[P].pin, value ? rpio.HIGH : rpio.LOW);
            this[P].value = value;

            process.nextTick(() => this.emit("change", value));
        }
    }

    return Led;
})();